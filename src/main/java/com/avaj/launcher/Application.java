package com.avaj.launcher;


import com.avaj.launcher.core.parser.Parser;
import com.avaj.launcher.core.parser.ParserException;
import com.avaj.launcher.core.simulator.Simulator;

import java.io.FileNotFoundException;

public class Application {

    public static void main(String[] args) {

        try {
            if (args.length != 1) {
                throw new IllegalArgumentException("Invalid number of arguments.");
            }
            Parser parser = new Parser(args[0]);
            parser.parse();
            Simulator simulator = new Simulator(parser.getIterations(), parser.getFlyables());
            simulator.run();
        } catch (IllegalArgumentException ex) {
            System.out.println(String.format("Error: %s", ex.getMessage()));
            System.out.println(String.format("Usage: java %s \033[4mfilename\033[0m\n", Application.class.getCanonicalName()));
        } catch (FileNotFoundException | ParserException ex) {
            System.out.println(String.format("Error: %s", ex.getMessage()));
        } catch (Exception ex) {
            for (int i = 0; i < args.length; i++) {
                System.out.println(String.format("args[%s]: %s", i, args[i]));
            }

            System.out.println("Exception occurred:");
            ex.printStackTrace(System.out);
        }

    }

}
