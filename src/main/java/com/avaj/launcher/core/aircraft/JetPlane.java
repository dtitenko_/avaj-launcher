package com.avaj.launcher.core.aircraft;

import com.avaj.launcher.core.weather.WeatherTower;

class JetPlane extends Aircraft implements Flyable {

    private WeatherTower weatherTower;

    JetPlane(String name, Coordinates coordinates) {
        super(name, coordinates);
    }

    public void updateConditions() {
        int longitude = 0, latitude = 0, height = 0;
        String weather = weatherTower.getWeather(coordinates);

        if ("SUN".equals(weather)) {
            latitude = 10;
            height = 2;
            log("Oh yeah! It's finally the good weather.");
        } else if ("RAIN".equals(weather)) {
            latitude = 5;
            log("It's raining. Better watch out for lightings.");
        } else if ("FOG".equals(weather)) {
            latitude = 1;
            log("Oh no! It's f*cking hell!");
        } else {
            height = -7;
            log("OMG! Winter is coming!");
        }

        incrCoordinates(longitude, latitude, height);

        if (coordinates.getHeight() == 0) {
            performLanding();
            weatherTower.unregister(this);
        }
    }

    public void registerTower(WeatherTower weatherTower) {
        this.weatherTower = weatherTower;
        weatherTower.register(this);
    }

    public String getName() {
        return getFullName();
    }
}
