package com.avaj.launcher.core.aircraft;

public class Coordinates {

    private Integer longitude;
    private Integer latitude;
    private Integer height;

    Coordinates(Integer longitude, Integer latitude, Integer height) {
        this.longitude = Math.max(0, longitude);
        this.latitude = Math.max(0, latitude);
        this.height = Math.max(0, Math.min(100, height));
    }

    public Integer getLongitude() {
        return longitude;
    }

    public Integer getLatitude() {
        return latitude;
    }

    public Integer getHeight() {
        return height;
    }
}
