package com.avaj.launcher.core.aircraft;

public class AircraftFactory {

    public static Flyable newAircraft(String type,
                                      String name,
                                      Integer longitude,
                                      Integer latitude,
                                      Integer height) throws IllegalArgumentException {

        Flyable flyable;
        Coordinates coordinates;

        if (latitude < 0 || longitude < 0 || height < 0) {
            throw new IllegalArgumentException("Coordinates should be positive");
        }

        coordinates = new Coordinates(longitude, latitude, height);

        if ("Baloon".equals(type)) {
            flyable = new Baloon(name, coordinates);
        } else if ("Helicopter".equals(type)) {
            flyable = new Helicopter(name, coordinates);
        } else if ("JetPlane".equals(type)) {
            flyable = new JetPlane(name, coordinates);
        } else {
            throw new IllegalArgumentException(String.format("Unknown type of aircraft: %s", type));
        }

        return flyable;
    }

}
