package com.avaj.launcher.core.aircraft;

import com.avaj.launcher.core.weather.WeatherTower;

class Helicopter extends Aircraft implements Flyable {

    private WeatherTower weatherTower;

    Helicopter(String name, Coordinates coordinates) {
        super(name, coordinates);
    }

    public void updateConditions() {
        int longitude = 0, latitude = 0, height = 0;
        String weather = weatherTower.getWeather(coordinates);

        if ("SUN".equals(weather)) {
            longitude = 10;
            height = 2;
            log("This is hot.");
        } else if ("RAIN".equals(weather)) {
            longitude = 5;
            log("LOL! I forgot my umbrella at home!");
        } else if ("FOG".equals(weather)) {
            longitude = 1;
            log("Oh! Visibility... is nowhere worse.");
        } else {
            height = -12;
            log("My rotor is going to freeze!");
        }

        incrCoordinates(longitude, latitude, height);

        if (coordinates.getHeight() == 0) {
            performLanding();
            weatherTower.unregister(this);
        }
    }

    public void registerTower(WeatherTower weatherTower) {
        this.weatherTower = weatherTower;
        weatherTower.register(this);
    }

    public String getName() {
        return getFullName();
    }
}
