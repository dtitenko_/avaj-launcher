package com.avaj.launcher.core.aircraft;

import com.avaj.launcher.core.weather.WeatherTower;

public interface Flyable {

    void updateConditions();
    void registerTower(WeatherTower weatherTower);
    String getName();

}
