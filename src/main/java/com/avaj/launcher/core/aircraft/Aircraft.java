package com.avaj.launcher.core.aircraft;

import com.avaj.launcher.core.Logger;

class Aircraft {

    private static final Logger logger = Logger.getInstance();
    private static Long idCounter = 0L;

    protected Long id;
    protected String name;
    protected Coordinates coordinates;

    protected Aircraft(String name, Coordinates coordinates) {
        this.id = nextId();
        this.name = name;
        this.coordinates = coordinates;
    }

    protected String getFullName() {
        return String.format("%s#%s(%s)", this.getClass().getSimpleName(), name, id);
    }

    protected void log(String msg) {
        logger.log(String.format("%s: %s", getFullName(), msg));
    }

    private Long nextId() {
        return ++idCounter;
    }

    protected void incrCoordinates(Integer longitude, Integer latitude, Integer height) {
        this.coordinates = new Coordinates(
            coordinates.getLongitude() + longitude,
            coordinates.getLatitude() + latitude,
            coordinates.getHeight() + height
        );
    }

    protected void performLanding() {
        if (coordinates.getHeight() == 0) {
            log(String.format("I'm landing. My coordinates is %s %s %s.",
                coordinates.getLongitude(),
                coordinates.getLatitude(),
                coordinates.getHeight()));
        }
    }
}
