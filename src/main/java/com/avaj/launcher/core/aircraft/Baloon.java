package com.avaj.launcher.core.aircraft;

import com.avaj.launcher.core.weather.WeatherTower;

class Baloon extends Aircraft implements Flyable {

    private WeatherTower weatherTower;

    Baloon(String name, Coordinates coordinates) {
        super(name, coordinates);
    }

    public void updateConditions() {
        int longitude = 0, latitude = 0, height = 0;
        String weather = weatherTower.getWeather(coordinates);

        if ("SUN".equals(weather)) {
            longitude = 2;
            height = 4;
            log("Let's enjoy the good weather and take some pics.");
        } else if ("RAIN".equals(weather)) {
            height = -5;
            log("Damn you rain! You messed up my baloon.");
        } else if ("FOG".equals(weather)) {
            height = -3;
            log("Damn. I can't see anything because of this fog.");
        } else {
            height = -15;
            log("It's snowing. We're  gonna crash.");
        }

        incrCoordinates(longitude, latitude, height);

        if (coordinates.getHeight() == 0) {
            performLanding();
            weatherTower.unregister(this);
        }
    }

    public void registerTower(WeatherTower weatherTower) {
        this.weatherTower = weatherTower;
        weatherTower.register(this);
    }

    public String getName() {
        return getFullName();
    }
}
