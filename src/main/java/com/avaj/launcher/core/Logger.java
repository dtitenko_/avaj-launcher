package com.avaj.launcher.core;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class Logger {

    private static final String OUTPUT_FILE = "simulation.txt";
    private static Logger logger;
    private final Writer writer;

    private Logger(Writer writer) {
        this.writer = writer;
    }

    public static Logger getInstance() {
        if (logger == null) {
            try {
                logger = new Logger(
                    new BufferedWriter(
                        new FileWriter(OUTPUT_FILE)));
            } catch (IOException ex) {
                System.out.println(String.format("Error: %s", ex.getMessage()));
            }
        }
        return logger;
    }

    public void log(String msg) {
        try {
            writer.write(msg);
            writer.write(System.lineSeparator());
        } catch (IOException ex) {
            System.out.print("Error: Cannot write, writer is closed");
            this.close();
        }
    }

    public void close() {
        try {
            writer.close();
        } catch (IOException ex) {
            System.out.println("Error: Cannot close writer");
            ex.printStackTrace(System.out);
        }
    }

}
