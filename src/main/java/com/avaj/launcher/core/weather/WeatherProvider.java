package com.avaj.launcher.core.weather;

import com.avaj.launcher.core.aircraft.Coordinates;

import java.util.Random;

public class WeatherProvider {

    private static WeatherProvider weatherProvider = new WeatherProvider();
    private static String[] weather = {
        "RAIN",
        "FOG",
        "SUN",
        "SNOW"
    };

    private WeatherProvider() {
    }

    public static WeatherProvider getProvider() {
        return weatherProvider;
    }

    public String getCurrentWeather(Coordinates coordinates) {
        int seed = coordinates.getLongitude() ^ coordinates.getLatitude() ^ coordinates.getHeight();
        seed += new Random().nextInt();
        return weather[Math.abs(seed) % 4];
    }

}
