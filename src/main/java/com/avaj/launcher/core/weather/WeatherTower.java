package com.avaj.launcher.core.weather;

import com.avaj.launcher.core.aircraft.Coordinates;

public class WeatherTower extends Tower {

    public String getWeather(Coordinates coordinates) {
        return WeatherProvider.getProvider().getCurrentWeather(coordinates);
    }

    void changeWeather() {
        conditionsChanged();
    }

    public void triggerChangeWeather() {
        changeWeather();
    }

}
