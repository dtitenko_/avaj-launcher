package com.avaj.launcher.core.weather;

import com.avaj.launcher.core.Logger;
import com.avaj.launcher.core.aircraft.Flyable;

import java.util.ArrayList;
import java.util.List;

public class Tower {

    private static final Logger logger = Logger.getInstance();

    private List<Flyable> observers = new ArrayList<>();
    private List<Flyable> unregistered = new ArrayList<>();

    public void register(Flyable flyable) {
        this.observers.add(flyable);
        log(String.format("%s registered to weather tower.", flyable.getName()));
    }

    public void unregister(Flyable flyable) {
        this.unregistered.add(flyable);
        log(String.format("%s unregistered from weather tower.", flyable.getName()));
    }

    protected void conditionsChanged() {
        for(Flyable flyable : observers) {
            flyable.updateConditions();
        }
        this.observers.removeAll(unregistered);
        unregistered.clear();
    }

    protected void log(String msg) {
        logger.log(String.format("Tower says: %s", msg));
    }
}
