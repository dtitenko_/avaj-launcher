package com.avaj.launcher.core.simulator;


import com.avaj.launcher.core.Logger;
import com.avaj.launcher.core.aircraft.Flyable;
import com.avaj.launcher.core.weather.WeatherTower;

import java.util.List;

public class Simulator {

    private final Integer iterations;
    private final List<Flyable> flyables;

    public Simulator(Integer iterations, List<Flyable> flyables) {
        this.iterations = iterations;
        this.flyables = flyables;
    }

    public void run() {
        try {
            if (flyables == null || flyables.isEmpty()) {
                throw new SimulatorException("No flying objects to simulate");
            }

            WeatherTower weatherTower = new WeatherTower();

            for (Flyable flyable: flyables) {
                flyable.registerTower(weatherTower);
            }

            for(int i = 0; i < iterations; i++) {
                weatherTower.triggerChangeWeather();
            }
        } catch (SimulatorException ex) {
            System.out.println(String.format("Error: %s", ex.getMessage()));
        } finally {
            Logger.getInstance().close();
        }
    }

}
