package com.avaj.launcher.core.simulator;

public class SimulatorException extends Exception {
    public SimulatorException() {}

    public SimulatorException(String message) {
        super(message);
    }

    public SimulatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public SimulatorException(Throwable cause) {
        super(cause);
    }
}
