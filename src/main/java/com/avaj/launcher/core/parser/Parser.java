package com.avaj.launcher.core.parser;

import com.avaj.launcher.core.aircraft.Flyable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import static com.avaj.launcher.core.aircraft.AircraftFactory.newAircraft;

public class Parser {

    private final String fileName;

    private List<Flyable> flyables;
    private Integer iterations;

    public Parser(String fileName) {
        this.fileName = fileName;
    }

    public void parse() throws ParserException, FileNotFoundException {
        if (!new File(fileName).exists()) {
            throw new FileNotFoundException();
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))){
            this.iterations = parseIterations(reader.readLine());
            flyables = new ArrayList<>();
            String line;
            while ((line = reader.readLine()) != null && !line.isEmpty()) {
                flyables.add(parseFlyable(line.split(" ")));
            }
        } catch (NumberFormatException ex) {
            throw new ParserException(String.format("invalid number format (%s)", ex.getMessage()));
        } catch (Exception ex) {
            throw new ParserException(String.format("invalid file (%s)", ex.getMessage()));
        }
    }

    private static Integer parseIterations(String str) throws NumberFormatException {
        if (str == null || str.isEmpty()) {
            throw new NumberFormatException("Invalid number of iterations");
        }
        int iterations = Integer.parseInt(str);
        if (iterations < 1) {
            throw new NumberFormatException("Number of iteration should be positive");
        }
        return iterations;
    }

    private static Flyable parseFlyable(String ...params) throws IllegalArgumentException {
        if (params.length != 5) {
            throw new IllegalArgumentException("Invalid number of parameters for Flyable objects");
        }
        int lon = Integer.parseInt(params[3]),
            lat = Integer.parseInt(params[2]),
            height = Integer.parseInt(params[4]);
        return newAircraft(params[0], params[1], lon, lat, height);
    }

    public List<Flyable> getFlyables() {
        return flyables;
    }

    public Integer getIterations() {
        return iterations;
    }

}
